var gulp = require('gulp');
var webpack = require('webpack');
var config = require('../config/webpack');
var browserSync = require('browser-sync');

gulp.task('webpack', function(cb) {
    webpack(config, function(err, stats) {
        cb();
    });
});

gulp.task('webpack:watch', function(cb){
    var built = false;
    webpack(config).watch(200, function(err, stats) {
        browserSync.reload();
        // On the initial compile, let gulp know the task is done
        if(!built) {
            built = true;
            cb();
        }
    })
});