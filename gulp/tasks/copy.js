var config      = require('../config/copy');
var gulp        = require('gulp');
var copy        = require('gulp-copy');

gulp.task('copy', function() {
    config.map(function(cfg) {
        gulp.src(cfg.src)
            .pipe(copy(cfg.dest, cfg.options));
    })
});
