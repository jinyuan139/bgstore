var browserSync     = require('browser-sync');
var gulp			= require('gulp');
var config			= require('../config/html');

gulp.task('html', function() {
    browserSync.init({
        browser: config.browser,
        server: config.src,
        port: config.port
    });

    browserSync.reload();
});

gulp.task('html:watch', function() {
    browserSync.init({
        browser: config.browser,
        server: config.src,
        port: config.port
    });

    gulp.watch( config.watch ).on('change', function(){
        browserSync.reload();
    });
});