var gulp				= require('gulp');
var sass				= require('gulp-sass');
var sourcemaps	        = require('gulp-sourcemaps');
var config			    = require('../config/sass');
var autoprefixer        = require('gulp-autoprefixer');
var fs                  = require('fs');
var browserSync         = require('browser-sync');

gulp.task('sass', function () {
    fs.readdir( config.srcThemes, function(err, list){
        list.map(function(themeDir){
            return gulp.src(config.srcThemes + '/' + themeDir + config.pattern)
                .pipe(sourcemaps.init())
                .pipe(sass().on('error', sass.logError))
                .pipe(autoprefixer(config.autoprefixer))
                .pipe(sourcemaps.write('./maps'))
                .pipe(gulp.dest(config.dest + '/themes/' + themeDir))
                .pipe(browserSync.stream());
        });
    });
    return gulp.src(config.src + '/*.*')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer(config.autoprefixer))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest(config.dest))
        .pipe(browserSync.stream());
});

gulp.task('sass:watch', function () {
    gulp.start('sass');
    gulp.watch( config.src + config.pattern , ['sass']);
});
