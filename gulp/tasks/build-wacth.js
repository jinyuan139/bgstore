var gulp = require('gulp');
var gulpSequence = require('gulp-sequence');

gulp.task('build:watch', function(cb){
    gulpSequence('clean', ['copy', 'jade:watch', 'sass:watch', 'webpack:watch'], 'html:watch', cb);
});