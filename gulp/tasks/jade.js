var jade = require('jade');
var gulpJade = require('gulp-jade');
var config = require('../config/jade');
var gulp = require('gulp');
var browserSync = require('browser-sync');

gulp.task('jade', function () {
    return gulp.src( config.src )
        .pipe(gulpJade({
            jade: jade,
            pretty: true
        }))
        .pipe(gulp.dest( config.dest ))
        .pipe(browserSync.stream());
});

gulp.task('jade:watch', function () {
    gulp.start('jade');
    gulp.watch( config.srcWatch , ['jade']);
});