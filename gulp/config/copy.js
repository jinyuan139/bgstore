var config = require('./');

module.exports = [
    {
        src: [
            './bower_components/jquery/**/*.*',
            './bower_components/respond/**/*.*',
            './bower_components/html5shiv/**/*.*',
            './bower_components/angular-motion/**/*.*',
            './bower_components/animate.css/**/*.*',
    
            './node_modules/bootstrap/**/*.*',
            './node_modules/normalize.css/**/*.*',
            './node_modules/font-awesome/**/*.*'
        ],
        dest: config.frontendDir + "/vendor",
        options: {
            prefix: 1
        }
    },
    {
        src: [
            config.assetsDir + '/**/*.*'
        ],
        dest: config.frontendDir,
        options: {
            prefix: 1
        }
    }
];