var config = {
    publishDir: './Publish',
    templateDir: './templates',
    nodeModulesDir: './node_modules'
};

// sources config
config.jadeDir = config.templateDir + '/jade';
config.sassDir = config.templateDir + '/sass';
config.sassThemeDir = config.sassDir + '/themes';
config.jsSrcDir = config.templateDir + '/js';
config.assetsDir = config.templateDir + '/assets';

// destination config
config.frontendDir = config.publishDir + '/frontend';
config.stylesDir = config.frontendDir + '/css';
config.jsDestDir = config.frontendDir + '/js';

module.exports = config;