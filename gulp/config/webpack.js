var config = require('./');
var path = require('path');

module.exports = {
    entry: {
        app: [config.jsSrcDir + "/app.js"]
    },
    resolve: {
        extensions: ['', '.js']
    },
    output: {
        path: config.jsDestDir,
        publicPath: "/js/",
        filename: "[name].js"
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                include: [
                    path.resolve(process.cwd(), config.jsSrcDir)
                ],
                query: {
                    presets: ['es2015']
                }
            }
        ]
    },
    devtool: 'source-map',
    debug: true
};