var config = require('./');

module.exports = {
    autoprefixer: { browsers: ['last 2 version'] },
    src: config.sassDir,
    srcThemes: config.sassThemeDir,
    dest: config.stylesDir,
    pattern: "/**/*.{sass,scss}",
    settings: {
        indentedSyntax: true, // Enable .sass syntax!
        imagePath: 'images' // Used by the image-url helper
    }
};