var config = require('./');

module.exports = {
    watch: config.frontendDir + '/*.html',
    src: [config.frontendDir],
    dest: config.publishDir,
    browser: 'chrome',
    port: 8000
};