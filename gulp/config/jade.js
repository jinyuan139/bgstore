var config = require('./');

module.exports = {
    srcWatch: [config.jadeDir + '/*.jade'],
    src: [config.jadeDir + '/*.jade', '!'+ config.jadeDir +'/_*.jade'],
    dest: config.frontendDir
};